<?php
/**
 * Template Name: Quem Somos
 * Description: 
 *
 * @package BSRA
 */
$fotoDestacada = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotoDestacada = $fotoDestacada[0];

//ÁREA DE TEXTO A 1 
$titulo_quemSomos1 =  rwmb_meta('Bsra_titulo_quemSomos1');
$descricao_quemSomos1 =  rwmb_meta('Bsra_descricao_quemSomos1');
$imagem_quemSomos1 =  rwmb_meta('Bsra_imagem_quemSomos1');

foreach ($imagem_quemSomos1 as $imagem_quemSomos1) {
	$imagem_quemSomos1 = $imagem_quemSomos1;
}


//ÁREA DE TEXTO A 2
$titulo_quemSomos2 =  rwmb_meta('Bsra_titulo_quemSomos2');
$descricao_quemSomos2 =  rwmb_meta('Bsra_descricao_quemSomos2');
$imagem_quemSomos2 =  rwmb_meta('Bsra_imagem_quemSomos2');

foreach ($imagem_quemSomos2 as $imagem_quemSomos2) {
	$imagem_quemSomos2 = $imagem_quemSomos2;
}

//ÁREA DE TEXTO A 3
$titulo_quemSomos3 =  rwmb_meta('Bsra_titulo_quemSomos3');
$descricao_quemSomos3 =  rwmb_meta('Bsra_descricao_quemSomos3');
$imagem_quemSomos3 =  rwmb_meta('Bsra_imagem_quemSomos3');

foreach ($imagem_quemSomos3 as $imagem_quemSomos3) {
	$imagem_quemSomos3 = $imagem_quemSomos3;
}

get_header(); ?>
	<!-- PÁGINA QUEM SOMOS -->
	<div class="pg pg-quamSomos">
		
		<!-- BANNER  -->
		<div class="bannerGeral" style="background:url(<?php echo $fotoDestacada ?>)">
			<div class="lenteBanner">
				<div class="bannerGeralInfor">
					<span><?php echo get_the_title() ?></span>
					<p><?php echo get_the_content() ?></p>
				</div>
			</div>
		</div>

		<!-- SESSÃO QUEM SOMOS -->
		<section class="sessaoQuemSomos">
			<div class="container">
				
				<!-- ÁREA DE TEXTO A 1 -->
				<div class="sessaoQuemSomosPosicaoA">
					
					<div class="row">
						<div class="col-md-9">
							<h5><?php echo $titulo_quemSomos1  ?></h5>

							<p><?php  echo $descricao_quemSomos1 ?></p>
						</div>
						<div class="col-md-3">
							<div class="sessaoQuemSomosPosicaoAFoto" style="background:url(<?php  echo $imagem_quemSomos1['full_url'] ?>)"></div>
						</div>
					</div>
				</div>

				<!-- ÁREA DE TEXTO B 2 -->
				<div class="sessaoQuemSomosPosicaoB">
					
					<div class="row">
						<div class="col-md-3">

							<div class="sessaoQuemSomosPosicaoBFoto" style="background:url(<?php echo $imagem_quemSomos2['full_url']; ?>)"></div>
						</div>

						<div class="col-md-9">
							<h5><?php  echo $titulo_quemSomos2 ?></h5>					

							<p><?php echo $descricao_quemSomos2 ?></p>

						</div>
					</div>
				</div>

				<!-- ÁREA DE TEXTO B 2 -->
				<div class="sessaoQuemSomosPosicaoB correcaoPosisao" style="display:none">
					
					<div class="row">

					<div class="col-md-9">
						<h5><?php  echo $titulo_quemSomos2 ?></h5>					

						<p><?php echo $descricao_quemSomos2 ?></p>

					</div>
					
					<div class="col-md-3">

						<div class="sessaoQuemSomosPosicaoBFoto" style="background:url(<?php echo $imagem_quemSomos2['full_url']; ?>)"></div>
					</div>

						
					</div>
				</div>

				<!-- ÁREA DE TEXTO A 3-->
				<div class="sessaoQuemSomosPosicaoA">
					
					<div class="row">
						<div class="col-md-9">
							<h5><?php echo $titulo_quemSomos3  ?></h5>

							<p><?php  echo $descricao_quemSomos3 ?></p>
						</div>
						<div class="col-md-3">
							<div class="sessaoQuemSomosPosicaoAFoto" style="background:url(<?php  echo $imagem_quemSomos3['full_url'] ?>)"></div>
						</div>
					</div>
				</div>

			</div>
		</section>

	</div>


<?php get_footer(); ?>