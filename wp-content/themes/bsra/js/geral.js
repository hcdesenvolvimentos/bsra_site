$(function(){


	/*****************************************
		CARROSSEL DE DESTAQUE
	*******************************************/
	$(document).ready(function() {

		$("#carrosselDestaque").owlCarousel({

			items : 1,
	        dots: true,
	        loop: true,
	        lazyLoad: true,
	        mouseDrag:false,
	        touchDrag  : false,	       
		    autoplayTimeout:5000,
		    autoplayHoverPause:true,
		    animateOut: 'fadeOut',
		    smartSpeed: 450,		    
		    center:true,		   
		    
		});
		var carrossel_destaque = $("#carrosselDestaque").data('owlCarousel');
		$('.carrosselDestaqueBtnF').click(function(){ carrossel_destaque.prev(); });
		$('.carrosselDestaqueBtnT').click(function(){ carrossel_destaque.next(); });



 	});


	
});