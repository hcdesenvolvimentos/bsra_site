<?php

/**

 * Template Name: Contato

 * Description: 

 *

 * @package Stetica

 */

$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

$foto = $foto[0];



$contato_frase =  rwmb_meta('Bsra_contato_frase');

$contato_nome =  rwmb_meta('Bsra_contato_nome');

get_header(); ?>



<div class="pg pg-contato" style="display:;">



		<!-- BANNER  -->

		<div class="bannerGeral" style="background:url(<?php echo $foto  ?>)">

			<div class="lenteBanner">

				<div class="bannerGeralInfor">

					<span><?php echo get_the_title() ?></span>

					<p><?php  echo $contato_frase ?></p>

				</div>

			</div>

		</div>



		<div class="container">	



			<section class="areaContato">

				

				<div class="col-md-12 esquerda">

					<img src="<?php bloginfo('template_directory'); ?>/img/iconeContato.png" alt="">

					<h3><?php  echo $contato_nome  ?></h3>

					<p class="texto">

						<?php //echo get_the_content() ?>

					</p>

				</div>



				<div class="col-md-12 direita">

					<div class="formulario">

						<?php

	                           echo do_shortcode('[contact-form-7 id="5" title="Formulário de contato 1"]');

	                        ?>

					</div>

				</div>



			</section>



		</div>

	</div>

	



<?php get_footer(); ?>