<?php

/**

 * The template for displaying all single posts.

 *

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post

 *

 * @package BSRA

 */

$fotoDestacada = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

$fotoDestacada = $fotoDestacada[0];

$tituloPOst = get_the_title();



get_header(); ?>



	<!-- ÁREAS DE ATUAÇÃO  -->

	<div class="pg pg-areaAtuacao">		

		

		<!-- BANNER  -->

		<div class="bannerGeral" style="background:url(<?php echo $fotoDestacada ?>)">

			<div class="lenteBanner">

				<div class="bannerGeralInfor">

					<span><?php echo get_the_title() ?></span>

					<p><?php echo get_the_content() ?></p>

				</div>

			</div>

		</div>

			

		<div class="areaConteudo">

			<!-- SIDEBAR  DE POST -->

			<section class="areaAtuacaoSidebar">

			<?php 

				$i = 0 ;

				// LOOP DE POST O QUE FAZEMOS 

				$areaatuacao = new WP_Query( array( 'post_type' => 'areaatuacao', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );

				while ( $areaatuacao->have_posts() ) : $areaatuacao->the_post();

					



					$tituloArea = get_the_title();

					// DESCRIÇÃO

					$descricao =  rwmb_meta('Bsra_areaatuacao_descricao');

					// LINK 

					$icone =  rwmb_meta('Bsra_areaatuacao_iconeprincipal');

					foreach ($icone as $icone) {

						$icone = $icone;

					}	

					if ($tituloPOst == $tituloArea ):

										

												

			?>

				<a href="<?php echo get_permalink(); ?>" class="ativo" title="<?php echo get_the_title() ?>" alt="<?php echo get_the_title() ?>"><?php echo get_the_title() ?></a>

			<?php  else: ?>

				<a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title() ?>" alt="<?php echo get_the_title() ?>"><?php echo get_the_title() ?></a>

			<?php  endif ?>

			<?php $i++; endwhile; wp_reset_query(); ?>

			</section>

		</div>



		<!-- POST -->

		<section class="areaAtuacaoPost">

			

			<div class="areaConteudo">

			<?php 

				



					// TÍTULO

					$tituloArea =  rwmb_meta('Bsra_areaatuacao_TituloeDescricao1');

					// ICONE 

					$iconeArea =  rwmb_meta('Bsra_areaatuacao_iconeDescricao1');

					// DESCRIÃO

					$descricaoArea =  rwmb_meta('Bsra_areaatuacao_descricaoDescricao1');

					

					$tituloFormat = explode(";", $tituloArea);





					foreach ($iconeArea as $iconeArea) {

						$iconeArea = $iconeArea;

					}	



					// TÍTULO

					$tituloArea2 =  rwmb_meta('Bsra_areaatuacao_TituloeDescricao2');

					// ICONE 

					$iconeArea2 =  rwmb_meta('Bsra_areaatuacao_iconeDescricao2');

					// DESCRIÃO

					$descricaoArea2 =  rwmb_meta('Bsra_areaatuacao_descricaoDescricao2');

					

					$tituloFormat2 = explode(";", $tituloArea2);



					foreach ($iconeArea2 as $iconeArea2) {

						$iconeArea2 = $iconeArea2;

					}	



					// TÍTULO

					$tituloArea3 =  rwmb_meta('Bsra_areaatuacao_TituloeDescricao3');

					// ICONE 

					$iconeArea3 =  rwmb_meta('Bsra_areaatuacao_iconeDescricao3');

					// DESCRIÃO

					$descricaoArea3 =  rwmb_meta('Bsra_areaatuacao_descricaoDescricao3');

					

					$tituloFormat3 = explode(";", $tituloArea3);



					foreach ($iconeArea3 as $iconeArea3) {

						$iconeArea3 = $iconeArea3;

					}	



				

										

												

			?>

				<div class="row">

					

					<div class="col-md-4">

						<div class="areaAtuacaoPostinfo">

							<h3><?php  echo $tituloFormat[0] ?> <span><?php  echo $tituloFormat[1] ?></span> </h3>
							<hr>
							<img src="<?php echo $iconeArea['full_url'] ?>" alt="<?php echo $tituloArea ?>" class="img-responsive">

							<?php echo $descricaoArea ?>

						</div>

					</div>



					<div class="col-md-4">

						<div class="areaAtuacaoPostinfo">

							<h3><?php  echo $tituloFormat2[0] ?> <span><?php  echo $tituloFormat2[1] ?></span></h3>
							<hr>
							 <img src="<?php echo $iconeArea2['full_url'] ?>" alt="<?php echo $tituloArea2 ?>" class="img-responsive">

							<?php echo $descricaoArea2 ?>

						</div>

					</div>



					<div class="col-md-4">

						<div class="areaAtuacaoPostinfo">

							<h3><?php  echo $tituloFormat3[0] ?> <span><?php  echo $tituloFormat3[1] ?></span> </h3>
							<hr>
							<img src="<?php echo $iconeArea3['full_url'] ?>" alt="<?php echo $tituloArea3 ?>" class="img-responsive">

							<?php echo $descricaoArea3 ?>

						</div>

					</div>



					



				</div>

					

				





			</div>

		</section>





	</div>	



<?php



get_footer();

