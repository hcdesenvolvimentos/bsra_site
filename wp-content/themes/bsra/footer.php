<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BSRA
 */
global $configuracao;

$telefone1 = $configuracao['opt-contato-telefone1'];
$telefone2 = $configuracao['opt-contato-telefone2'];
$telefone3 = $configuracao['opt-contato-telefone3'];

$endereco1 = $configuracao['opt-endereco1'];
$endereco2 = $configuracao['opt-endereco2'];
$endereco3 = $configuracao['opt-endereco3'];
$linkedin = $configuracao['opt-linkedin'];
$facebook = $configuracao['opt-facebook'];
$twitter = $configuracao['opt-twitter'];



?>

	
	<div class="mapa">
	<!-- <div class="lente"></div>  -->
		<div class="areaMapa" id="map"></div>
		<div class="areaBtnMapa">
			<a href="<?php echo $enderecoGoogle; ?>" target="_blank" title="Detecta no Google Maps" class="btnMapa"></a>
		</div>

	</div>


	<!-- FOOTER -->
	<footer class="rodape">
		
		<ul>

			<li>
				<div class="rodapeLinks">
					<span>Inicio</span>
					<a href="<?php echo home_url('/quem-somos/'); ?>">Quem somos</a>
					<a href="<?php echo home_url('areas-de-atuacao/'); ?>">Área de atuação</a>
					<a href="<?php echo home_url('/'); ?>">Compromisso social</a>
					<a href="<?php echo home_url('/blog/'); ?>">Blog</a>
					<a href="<?php echo home_url('/contato/'); ?>">Contato</a>
				</div>
			</li>

			<li>
				
				<div class="rodapeEndereco">
					<?php $cidade1 = explode("/", $endereco1) ;; ?>
					<span><?php echo $cidade1[0] ?></span>
					<a href="https://www.google.com.br/maps/place/<?php echo  $cidade1[0] ; echo $cidade1[1]  ?>" target="_blank"><?php echo  $cidade1[1]; echo $telefone1   ?></a>	
					
				</div>

				<div class="rodapeEndereco">
					<?php $cidade2 = explode("/", $endereco2) ?>
					<span><?php echo $cidade2[0] ?></span>
					<a href="https://www.google.com.br/maps/place/<?php echo  $cidade2[0] ; echo $cidade2[1]  ?>" target="_blank"><?php echo  $cidade2[1]; echo $telefone2   ?></a>	
					
				</div>

				<div class="rodapeEndereco">
					<?php $cidade3 = explode("/", $endereco3) ?>
					<span><?php echo $cidade3[0] ?></span>
					<a href="https://www.google.com.br/maps/place/<?php echo  $cidade3[0] ; echo $cidade3[1]  ?>" target="_blank"><?php echo  $cidade3[1]; echo $telefone3   ?></a>	
					
				</div>


			</li>

			<li>

				<div class="rodapeRedessociais">
					<span>Nossas redes</span>
					<a href="<?php echo $facebook ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					<a href="<?php echo $twitter ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					<a href="<?php echo $linkedin ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
				</div>

			</li>

		</ul>
		
	</footer>

	<div class="copyright">		
		
		<p><i class="fa fa-copyright" aria-hidden="true"></i> Copyryght 2016 - Bernardes, Silva & Rabello Todos os direitos reservados	</p>
		
	</div>
	<small class="desenvolvidoPalupa">
		<span>Desenvolvido por </span>
		<a href="http://palupa.com.br/" target="_blank" title="Desenvolvido por Palupa Marketing"><img src="<?php bloginfo('template_directory'); ?>/img/palupaLogo.png"></a>
	</small>
<?php  $valor = str_replace("/", "", $endereco1); 

?>
<?php wp_footer(); ?>
<?php 
	
	// FUNÇÃO MAPA 
	$geo   = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($endereco1).'&sensor=false');
	$geo   = json_decode($geo, true);

	if ($geo['status'] == 'OK') {
		$latitude = $geo['results'][0]['geometry']['location']['lat'];
		$longitude = $geo['results'][0]['geometry']['location']['lng'];
	}


 ?>
	<!-- FUNÇÃO PARA ADICIONAR ICONE NO MAPA -->
	<script>

		var map;
		//POSICIONAMENTO DOS ESTABALECIMENTOS
		var endereco = {lat: <?php echo $latitude; ?>, lng: <?php echo $longitude; ?>};


		//ICONES DOS ESTABELECIMENTOS
		var icone1 = '<?= get_bloginfo("template_url"); ?>' + '/img/pin.png';
		
		
		function initMap() {

			//MAPA
			map = new google.maps.Map(document.getElementById('map'), {
				center: endereco,
				zoom: 15,
				//disableDefaultUI: true,
				zoomControl: true,
				scrollwheel: false,
				draggable: false, 
				disableDoubleClickZoom: true,
				
			});

			//MARKERS OU PINS DO MAPA
			var enderecoMap1 = new google.maps.Marker({
			    position: endereco,
			    icon: icone1,
			    map: map
			});

			google.maps.event.addListener(enderecoMap1, 'click', function(e) {
				window.open(enderecoMapa1);
				
			});




			// LENTE MAPA

			var overlay;
			USGSOverlay.prototype = new google.maps.OverlayView();


			var bounds = new google.maps.LatLngBounds(
			new google.maps.LatLng(<?php echo $latitude2; ?>),
			new google.maps.LatLng(<?php echo $longitude2; ?>));

			var srcImage = '<?= get_bloginfo("template_url"); ?>' + '/img/lente.png';

			overlay = new USGSOverlay(bounds, srcImage, map);
			console.log(overlay);


		}
		function USGSOverlay(bounds, image, map) {

		  // Initialize all properties.
		  this.bounds_ = bounds;
		  this.image_ = image;
		  this.map_ = map;

		  // Define a property to hold the image's div. We'll
		  // actually create this div upon receipt of the onAdd()
		  // method so we'll leave it null for now.
		  this.div_ = null;

		  // Explicitly call setMap on this overlay.
		  this.setMap(map);

		  USGSOverlay.prototype.onAdd = function() {

			var div = document.createElement('div');
			div.style.borderStyle = 'none';
			div.style.borderWidth = '0px';
			div.style.width = '100%';
		  	div.style.height = '500px';
			div.style.position = 'absolute';

			  // Create the img element and attach it to the div.
			  var img = document.createElement('img');
			  img.src = this.image_;
			  img.style.width = '100%';
			  img.style.height = '100%';
			  img.style.position = 'absolute';
			  div.appendChild(img);

		  console.log(div);
			  this.div_ = div;

			  // Add the element to the "overlayLayer" pane.
			  var panes = this.getPanes();
			  panes.overlayLayer.appendChild(div);
			};

			USGSOverlay.prototype.draw = function() {

			  // We use the south-west and north-east
			  // coordinates of the overlay to peg it to the correct position and size.
			  // To do this, we need to retrieve the projection from the overlay.
			  var overlayProjection = this.getProjection();

			  // Retrieve the south-west and north-east coordinates of this overlay
			  // in LatLngs and convert them to pixel coordinates.
			  // We'll use these coordinates to resize the div.
			  var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
			  var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());

			  // Resize the image's div to fit the indicated dimensions.
			  var div = this.div_;
			  div.style.left = sw.x + 'px';
			  //div.style.top = ne.y + 'px';
			  div.style.width = (ne.x - sw.x) + 'px';
			  div.style.height = (sw.y - ne.y) + 'px';
			};
		}





    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBY7oA-HMU8HYmWtlQ1nJxWsWLTtpctYt0&callback=initMap" async defer></script>
</body>
</html>
