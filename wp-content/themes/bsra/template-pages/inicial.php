<?php

/**

 * Template Name: Inicial

 * Description: 

 *

 * @package BSRA

 */



	// META ÁREA COMPROMISSO SOCIAL

	$inicialCaixatexto_tiluto = rwmb_meta('Bsra_inicialCaixatexto_tiluto');

	$inicialCaixatexto_descricao = rwmb_meta('Bsra_inicialCaixatexto_descricao');

	$inicialCaixatexto_iamgem = rwmb_meta('Bsra_inicialCaixatexto_iamgem');

	$inicialCaixatexto_Link = rwmb_meta('Bsra_inicialCaixatexto_Link');

	$inicialblog_titluto = rwmb_meta('Bsra_inicialblog_titluto');

	$inicialblog_texto = rwmb_meta('Bsra_inicialblog_texto');



	foreach ($inicialCaixatexto_iamgem as $inicialCaixatexto_iamgem) {

		$inicialCaixatexto_iamgem =	$inicialCaixatexto_iamgem;

	}

get_header(); ?>



<!-- PÁGINA INICIAL -->

<div class="pg pg-inicial">

	

	<!-- CARROSSEL DESTAQUE -->

	<section class="carrosselDestaque">			



		<div id="carrosselDestaque" class="owl-Carousel">  

			<?php 

				// LOOP DE POST DESTAQUE

				$destaques = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );

				while ( $destaques->have_posts() ) : $destaques->the_post();

					$fotoDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

					$fotoDestaque = $fotoDestaque[0];



					// DESCRIÇÃO

					$descricao =  rwmb_meta('Bsra_destaque_descricao');

					// LINK 

					$link =  rwmb_meta('Bsra_destaque_link');



			?>			

			<div class="item" style="background:url(<?php echo $fotoDestaque ?>)">

				<div class="carrosselDestaqueLente">		

					<div class="carrosselDestaqueInformacoes">

						<!-- TÍTULO -->

						<h2><?php echo get_the_title() ?></h2>



						<!-- DESCRIÇÃO -->

						<p><?php echo $descricao ?></p>

						

						<!-- LINK -->

						<a href="<?php echo $link ?>" alt="<?php echo get_the_title() ?>" target="_blank" > < saiba mais > </a>

					</div>			

				</div>

			</div>

			<?php endwhile; wp_reset_query(); ?>		



			

		</div>  

	

		<!-- BOTÕES -->

		<button class="carrosselDestaqueBtnF"><i class="fa  fa-angle-left"></i></button>

		<button class="carrosselDestaqueBtnT"><i class="fa  fa-angle-right"></i></button>



	</section>	



	

		<div class="titulo">

			<span class="tituloGerais">O que fazemos ?</span>

		</div>

		<!-- SESSÃO O QUE FAZEMOS -->

		<section class="sessaoOqueFazemos">

			

			<ul>

			<?php 

				// LOOP DE POST O QUE FAZEMOS 

				$areaatuacao = new WP_Query( array( 'post_type' => 'areaatuacao', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );

				while ( $areaatuacao->have_posts() ) : $areaatuacao->the_post();

					$fotoareaatuacao = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

					$fotoareaatuacao = $fotoareaatuacao[0];



					// DESCRIÇÃO

					$descricao =  rwmb_meta('Bsra_areaatuacao_descricao');

					// LINK 

					$icone =  rwmb_meta('Bsra_areaatuacao_iconeprincipal');

					foreach ($icone as $icone) {

						$icone = $icone;

					}					

			?>			

				<li>

					<!-- <img src="<?php //echo $icone['full_url']  ?>" class="img-responsive" alt=""> -->

					<h2><?php echo get_the_title() ?></h2>

					<p><?php echo $descricao ?></p>

					<a href="<?php echo get_permalink(); ?>">saiba mais</a>

				</li>				

			<?php endwhile; wp_reset_query(); ?>	

			</ul>



		</section>



		<!-- SESSÃO BANNER -->

		<section class="sessaoBanner" style="background:url(<?php echo $inicialCaixatexto_iamgem['full_url'] ?>)">

		

			<a href="<?php echo $inicialCaixatexto_Link ?>" target="_blank" alt="<?php echo $inicialCaixatexto_tiluto ?>" class="areaTexto">

				<h3><?php echo $inicialCaixatexto_tiluto ?></h3>

				<p><?php  echo $inicialCaixatexto_descricao ?></p>

				

			</a>



		</section>



		<!-- SESSÃO DE PUBLICAÇÕES DO BLOG  -->

		<section class=" publicacoesBlog">

			

			<div class="row">

				

				<div class="col-md-5">

					

					<div class="publicacoesBlogInfo">

						<span><?php echo $inicialblog_titluto ?></span>

						<p><?php echo $inicialblog_texto ?></p>



					</div>



				</div>



				<div class="col-md-7">

					

					<div class="publicacoesBlogAreaPost">

						<ul>

						<?php 

							// LOOP DE POST

							$posts = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -3) );

							while ( $posts->have_posts() ) : $posts->the_post();

								$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

								$fotoPost = $fotoPost[0];



								// DESCRIÇÃO

								$descricao =  rwmb_meta('Bsra_destaque_descricao');

								// LINK 

								$link =  rwmb_meta('Bsra_destaque_link');



						?>			

							<li>

								<a href="<?php echo get_permalink(); ?>">

									<span><?php the_time('j \d\e F \d\e Y') ?>  </span>

									<h2> <i><?php echo get_the_title() ?></i><?php customExcerpt(50); ?></h2>

								</a>

							</li>

 

						<?php endwhile; wp_reset_query(); ?>	

						</ul>

						

						<a href="<?php echo home_url('/blog/'); ?>" class="verMais">ver mais</a>

					</div>



				</div>



			</div>



		</section>

	

</div>



<?php get_footer(); ?>