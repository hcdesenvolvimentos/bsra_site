<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BSRA
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php 
	if (has_post_thumbnail()){
	$fotoMeta = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$fotoMeta = $fotoMeta[0];
 ?>
<meta property="og:title" content="BSRA" />
<meta property="og:description" content="<?php echo get_the_title() ?> 	- <?php $textoresumido = get_the_content(); $textocurto = substr($textoresumido, 0, 350).'...'; echo strip_tags($textocurto); ?>	" />
<meta property="og:url" content="" />
<meta property="og:image" content="<?php echo $fotoMeta ?>"/>
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<?php }?>
<?php wp_head(); ?>

<?php wp_head(); ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67616006-22', 'auto');
  ga('send', 'pageview');

</script>
</head>
<!-- FAVICON -->
<link rel="shortcut icon" type="image/x-icon" href=" <?php echo get_template_directory_uri(); ?>/favicon.png">
<body <?php body_class(); ?>>
<!-- TOPO -->
<header class="topo">
	<div class="areaMenu">
		<div class="row">
			<div class="col-md-3 logo">
				<a href="<?php echo home_url('/'); ?>">
					<h1 class="logoTop">BSRA</h1>	
				</a>

			</div>
			<div class="col-md-9">
				<!-- MENU  -->		
				<div class="navbar" role="navigation">	
									
					<!-- MENU MOBILE TRIGGER -->
					<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
						<span class="sr-only"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<!--  MENU MOBILE-->
					<div class="row navbar-header">			

						<nav class="collapse navbar-collapse" id="collapse">

							<ul class="nav navbar-nav">			
								<li><a href="<?php echo home_url('/'); ?>" id="inicial" class="linkSessao" title="Inicio">Inicio</a></li>						
								<li><a href="<?php echo home_url('/quem-somos/'); ?>" id="quem-somos" class="linkSessao" title="Quem Somos" clas>Quem Somos</a></li>		
								<li><a href="<?php echo home_url('areas-de-atuacao/'); ?>" id="area-atuacao" class="linkSessao" title="àreas de atuação">áreas de atuação</a></li>																			
								<li><a href="<?php echo home_url('/nossos-advogados/'); ?>" id="profissionais" title="nossos-advogados">Profissionais</a></li>																			
								<li><a href="<?php echo home_url('/blog/'); ?>" id="blog" title="Blog">Blog</a></li>																			
								<li><a href="<?php echo home_url('/contato/'); ?>" id="contato" title="Contato">Contato</a></li>																			
								
							</ul>

						</nav>						

					</div>			
					
				</div>
			</div>
		</div>	
	</div>
</header>

