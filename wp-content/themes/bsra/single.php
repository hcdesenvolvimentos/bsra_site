<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package BSRA
 */


get_header(); ?>

<div class="pg pg-blog">

	
	<!-- BANNER  -->
	<div class="bannerGeral" style="background:url(<?php  echo $configuracao['opt-blog-foto']['url'] ?>)">
		<div class="lenteBanner">
			<div class="bannerGeralInfor">
				<span><?php  echo $configuracao['opt-blog-titulo'] ?></span>
				<p><?php  echo $configuracao['opt-blog-frase'] ?></p>
			</div>
		</div>
	</div>

	<div class="container areaPostagem">	
		<section class="row">				
			
			<!-- SIDEBAR -->
			<div class="col-lg-4 col-md-3 sidebar">
				<img src="<?php bloginfo('template_directory'); ?>/img/calendar.jpg" alt="" class="img-responsive">
				<ul class="listaCategorias" style="margin-top: 63px;">
					<?php
						// CATEGORIA ATUAL
						$categoriaAtual = get_the_category();
						$categoriaAtual = $categoriaAtual[0]->cat_name;
						// LISTA DE CATEGORIAS
						$arrayCategorias = array();
						$categorias=get_categories($args);

						foreach($categorias as $categoria) {
							$arrayCategorias[$categoria->cat_ID] = $categoria->name;
							$nomeCategoria = $arrayCategorias[$categoria->cat_ID];

							if ($categoriaAtual == $nomeCategoria ) {
								
							
					?>
						<li class="categoria "><a class="ativa" href="<?php echo get_category_link($categoria->cat_ID); ?>"><?php echo $nomeCategoria; ?></a></li>

					<?php }else{ ?>
							<li class="categoria"><a href="<?php echo get_category_link($categoria->cat_ID); ?>"><?php echo $nomeCategoria; ?></a></li>

					<?php }} ?>


				</ul>
			</div>

			<!-- POSTAGEM -->
			<div class="col-md-8 areaPostagem">

			<?php 	if ( have_posts() ) : 

						while ( have_posts() ) : the_post();
					
	                        $foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	                        $foto = $foto[0];
		 	?>
							<!-- CAMINHO DA PÁGINA -->
							<div class="caminho">Blog &gt; <?php $categoriaAtual = get_the_category();
						 echo $categoriaAtual = $categoriaAtual[0]->cat_name; ?></div>

							<article>

								<div class="foto" style="background:url(<?=$foto?>)"></div>

								<div class="texto">
									<h4><?=the_title()?></h4>					
									<p class="data"><?php the_time('j F \d\e Y') ?></p>
									<p class="conteudo">
										<?php echo get_the_content(); ?>
									</p>
								</div>
									
							</article>

							<a href="" class="facebook"></a>
							<a href="" class="twitter"></a>

							<div class="outrosPosts">
								<?php previous_post('%','&lt; Anterior', 'no') ?>
								<?php next_post('%','Próximo &gt;', 'no') ?>
							</div>			
						

			<?php 		endwhile; ?>  

			<?php 	endif; ?>
			<div class=" disqus">
				<?php
					//If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>				
			</div>
		</div>
			
		</section>
	</div>		
</div>


<?php

get_footer();
