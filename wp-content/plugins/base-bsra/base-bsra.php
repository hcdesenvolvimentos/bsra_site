<?php

/**
 * Plugin Name: Base BSRA Advogados e Consultores
 * Description: Controle base do tema BSRA Advogados e Consultores.
 * Version: 0.1
 * Author: Agência Palupa
 * Author URI: http://www.palupa.com.br
 * Licence: GPL2
 */


function baseBsra () {

		// TIPOS DE CONTEÚDO
		conteudosBsra();

		// TAXONOMIA
		//taxonomiaBsra();

		// META BOXES
		metaboxesBsra();

	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosBsra (){

		// TIPOS DE CONTEÚDO

		tipoDestaque();

		tipoAreAtuacao();

		tipoprofissionais();
		

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do Destaque';
				break;

				case 'areaatuacao':
					$titulo = 'nome da área de atuação';
				break;

				case 'profissional':
					$titulo = 'nome do profissional';
				break;
			}

		    return $titulo;

		}

	}

		// CUSTOM POST TYPE DESTAQUES
		function tipoDestaque() {

			$rotulosDestaque = array(
									'name'               => 'Destaque',
									'singular_name'      => 'destaque',
									'menu_name'          => 'Destaques',
									'name_admin_bar'     => 'Destaques',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo destaque',
									'new_item'           => 'Novo destaque',
									'edit_item'          => 'Editar destaque',
									'view_item'          => 'Ver destaque',
									'all_items'          => 'Todos os destaque',
									'search_items'       => 'Buscar destaque',
									'parent_item_colon'  => 'Dos destaque',
									'not_found'          => 'Nenhum destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum destaque na lixeira.'
								);

			$argsDestaque 	= array(
									'labels'             => $rotulosDestaque,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'destaque' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('destaque', $argsDestaque);

		}

		// CUSTOM POST TYPE ÁREAS DE ATUAÇÃO
		function tipoAreAtuacao() {

			$rotulosAreAtuacao = array(
									'name'               => 'Área de atuação',
									'singular_name'      => 'área de atuação',
									'menu_name'          => 'Áreas de atuação',
									'name_admin_bar'     => 'áreas de atuação',
									'add_new'            => 'Adicionar nova',
									'add_new_item'       => 'Adicionar nova área de atuação',
									'new_item'           => 'Nova área de atuação',
									'edit_item'          => 'Editar área de atuação',
									'view_item'          => 'Ver área de atuação',
									'all_items'          => 'Todas as áreas de atuação',
									'search_items'       => 'Buscar área de atuação',
									'parent_item_colon'  => 'Das áreas de atuação',
									'not_found'          => 'Nenhuma área de atuação cadastrado.',
									'not_found_in_trash' => 'Nenhuma área de atuação na lixeira.'
								);

			$argsAreAtuacao	= array(
									'labels'             => $rotulosAreAtuacao,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-screenoptions',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'area-de-atuacao' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('areaatuacao', $argsAreAtuacao);

		}

		// CUSTOM POST TYPE PROFICIONAIS
		function tipoprofissionais() {

			$rotulosprofissionais = array(
									'name'               => 'Profissional',
									'singular_name'      => 'profissional',
									'menu_name'          => 'Profissionais',
									'name_admin_bar'     => 'profissionais',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo profissional',
									'new_item'           => 'Novo profissional',
									'edit_item'          => 'Editar profissional',
									'view_item'          => 'Ver profissional',
									'all_items'          => 'Todos os profissionais',
									'search_items'       => 'Buscar profissional',
									'parent_item_colon'  => 'Dos profissionais',
									'not_found'          => 'Nenhum profissional cadastrado.',
									'not_found_in_trash' => 'Nenhum profissional na lixeira.'
								);

			$argsprofissionais	= array(
									'labels'             => $rotulosprofissionais,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-groups',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'profissionais' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('profissionais', $argsprofissionais);

		}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
	// function taxonomiaBsra () {

	// 	taxonomiaCategoriaCardapio();
	// }
	
	// 	// TAXONOMIA DE CARDÁPIO
	// 	function taxonomiaCategoriaCardapio() {

	// 		$rotulosCategoriaCardapio = array(
	// 											'name'              => 'Categorias de cardápio',
	// 											'singular_name'     => 'Categoria de cardápio',
	// 											'search_items'      => 'Buscar categorias de cardápio',
	// 											'all_items'         => 'Todas as categorias de cardápio',
	// 											'parent_item'       => 'Categoria de cardápio pai',
	// 											'parent_item_colon' => 'Categoria de cardápio pai:',
	// 											'edit_item'         => 'Editar categoria de cardápio',
	// 											'update_item'       => 'Atualizar categoria de cardápio',
	// 											'add_new_item'      => 'Nova categoria de cardápio',
	// 											'new_item_name'     => 'Nova categoria',
	// 											'menu_name'         => 'Categorias de cardápio',
	// 										);

	// 		$argsCategoriaCardapio 		= array(
	// 											'hierarchical'      => true,
	// 											'labels'            => $rotulosCategoriaCardapio,
	// 											'show_ui'           => true,
	// 											'show_admin_column' => true,
	// 											'query_var'         => true,
	// 											'rewrite'           => array( 'slug' => 'categoria-cardapio' ),
	// 										);

	// 		register_taxonomy( 'categoriaCardapio', array( 'cardapio' ), $argsCategoriaCardapio );

	// 	}

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesBsra(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Bsra_';

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxDestaque',
				'title'			=> 'Detalhes do Destaque',
				'pages' 		=> array( 'destaque' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(			
					
					
					array(
						'name'  => 'Descrição: ',
						'id'    => "{$prefix}destaque_descricao",
						'desc'  => '',
						'type'  => 'textarea',
						
					),	
					array(
						'name'  => 'Link do destaque: ',
						'id'    => "{$prefix}destaque_link",
						'desc'  => '',
						'type'  => 'text',
						
					),				
					
				),
				
			);

		 	/****************************************************
			* META BOXES ÁREA DE ATUAÇÃO
			*****************************************************/

			// METABOX DE ÁREAS DE ATUAÇÃO ICONE PRINCIPAL
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxareaatuacao',
				'title'			=> 'Detalhes da área de atuação - Icone principal',
				'pages' 		=> array( 'areaatuacao' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
					
					array(
						'name'  => 'Icone principal: ',
						'id'    => "{$prefix}areaatuacao_iconeprincipal",
						'desc'  => 'Icone da página inicial',
						'type'  => 'image',
						'max_file_uploads'  => 1,						
					),		
					
					array(
						'name'  => 'Descrição: ',
						'id'    => "{$prefix}areaatuacao_descricao",
						'desc'  => '',
						'type'  => 'text',
						
					),						
					
				),
				
			);	

			// METABOX DE ÁREAS DE ATUAÇÃO DESCRIÇÃO 1
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxareaatuacaodescricao1',
				'title'			=> 'Detalhes da área de atuação - Primeira descrição',
				'pages' 		=> array( 'areaatuacao' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(	

					array(
						'name'  => 'Título: ',
						'id'    => "{$prefix}areaatuacao_TituloeDescricao1",
						'desc'  => '',
						'type'  => 'text',
											
					),		
						
					array(
						'name'  => 'Icone: ',
						'id'    => "{$prefix}areaatuacao_iconeDescricao1",
						'desc'  => 'Icone da página do post',
						'type'  => 'image',
						'max_file_uploads'  => 1,						
					),	
					array(
						'name'  => 'Descrição: ',
						'id'    => "{$prefix}areaatuacao_descricaoDescricao1",
						'desc'  => '',
						'type'  => 'wysiwyg',
						
					),						
					
				),
				
			);

			// METABOX DE ÁREAS DE ATUAÇÃO DESCRIÇÃO 2
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxareaatuacaodescricao2',
				'title'			=> 'Detalhes da área de atuação - Segunda descrição',
				'pages' 		=> array( 'areaatuacao' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(	

					array(
						'name'  => 'Título: ',
						'id'    => "{$prefix}areaatuacao_TituloeDescricao2",
						'desc'  => '',
						'type'  => 'text',
											
					),		
						
					array(
						'name'  => 'Icone: ',
						'id'    => "{$prefix}areaatuacao_iconeDescricao2",
						'desc'  => 'Icone da página do post',
						'type'  => 'image',
						'max_file_uploads'  => 1,						
					),	
					array(
						'name'  => 'Descrição: ',
						'id'    => "{$prefix}areaatuacao_descricaoDescricao2",
						'desc'  => '',
						'type'  => 'wysiwyg',
						
					),						
					
				),
				
			);

			// METABOX DE ÁREAS DE ATUAÇÃO DESCRIÇÃO 1
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxareaatuacaodescricao3',
				'title'			=> 'Detalhes da área de atuação - Terceira descrição',
				'pages' 		=> array( 'areaatuacao' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(	

					array(
						'name'  => 'Título: ',
						'id'    => "{$prefix}areaatuacao_TituloeDescricao3",
						'desc'  => '',
						'type'  => 'text',
											
					),		
						
					array(
						'name'  => 'Icone: ',
						'id'    => "{$prefix}areaatuacao_iconeDescricao3",
						'desc'  => 'Icone da página do post',
						'type'  => 'image',
						'max_file_uploads'  => 1,						
					),	
					array(
						'name'  => 'Descrição: ',
						'id'    => "{$prefix}areaatuacao_descricaoDescricao3",
						'desc'  => '',
						'type'  => 'wysiwyg',
						
					),						
					
				),
				
			);

			// METABOX DE PROFICIONAIS
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxprofissionais',
				'title'			=> 'Detalhes do profissional',
				'pages' 		=> array( 'profissionais' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(	

					array(
						'name'  => 'Descrição do Advogado (a): ',
						'id'    => "{$prefix}profissionais_infra",
						
						'type'  => 'text',
						'placeholder'  => 'Descrição do procissional',
											
					),

					array(
						'name'  => 'Link para o facebook: ',
						'id'    => "{$prefix}profissionais_facebook",
						'desc'  => '',
						'type'  => 'text',
						'placeholder'  => 'https://www.facebook.com/',
											
					),
					array(
						'name'  => 'Link para o linkedin: ',
						'id'    => "{$prefix}profissionais_linkedin",
						'desc'  => '',
						'type'  => 'text',
						'placeholder'  => 'https://www.linkedin.com/',
											
					),

					
				),
				
			);

			/****************************************************
			* META BOXES PÁGINAS
			*****************************************************/
			// METABOX INICIAL
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxpaginaInicial',
				'title'			=> 'Detalhes da página inicial : Caixa de texto a baixo dos itens o que fazemos | Informações do blog :Nossas Publicações',				
				'pages' 		=> array( 'page' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(	

					array(
						'name'  => 'Título : ',
						'id'    => "{$prefix}inicialCaixatexto_tiluto",
						'desc'  => '',
						'type'  => 'text',
						'placeholder'  => 'Compromisso Social',
											
					),
					
					array(
						'name'  => 'Descrição: ',
						'id'    => "{$prefix}inicialCaixatexto_descricao",
						'desc'  => '',
						'type'  => 'textarea',
						'placeholder'  => 'Breve descrição',
											
					),
					
					array(
						'name'  => 'Imagem: ',
						'id'    => "{$prefix}inicialCaixatexto_iamgem",
						
						'type'  => 'image',
						'max_file_uploads'  => 1,						
					),
					array(
						'name'  => 'Link: ',
						'id'    => "{$prefix}inicialCaixatexto_Link",
						'desc'  => '',
						'type'  => 'text',
						'placeholder'  => '#',
											
					),
					array(
						'name'  => 'Título área blog: ',
						'id'    => "{$prefix}inicialblog_titluto",
						'desc'  => '',
						'type'  => 'text',
						
											
					),
					array(
						'name'  => 'Descrição área blog: ',
						'id'    => "{$prefix}inicialblog_texto",
						'desc'  => '',
						'type'  => 'text',
						
											
					),

					
				),
				
			);

			// METABOX QUEM SOMOS ÁREA 1
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxQuemSomos',
				'title'			=> 'Detalhes da página primeira área de texto ',				
				'pages' 		=> array( 'page' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					
					array(
						'name'  => 'Primeira área de texto: Título ',
						'id'    => "{$prefix}titulo_quemSomos1",
						'desc'  => '',
						'type'  => 'text',
						'placeholder'  => 'Título',
											
					),

					array(
						'name'  => 'Primeira área de texto: Texto ',
						'id'    => "{$prefix}descricao_quemSomos1",
						'desc'  => '',
						'type'  => 'textarea',
						'placeholder'  => 'Descrição',
											
					),
					
					array(
						'name'  => 'Imagem: ',
						'id'    => "{$prefix}imagem_quemSomos1",						
						'type'  => 'image',
						'max_file_uploads'  => 1,						
					),				
					
				),
				
			);

			// METABOX QUEM SOMOS ÁREA 2
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxQuemSomos2',
				'title'			=> 'Detalhes da página segunda área de texto ',				
				'pages' 		=> array( 'page' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					
					array(
						'name'  => 'segunda área de texto: Título ',
						'id'    => "{$prefix}titulo_quemSomos2",
						'desc'  => '',
						'type'  => 'text',
						'placeholder'  => 'Título',
											
					),

					array(
						'name'  => 'segunda área de texto: Texto ',
						'id'    => "{$prefix}descricao_quemSomos2",
						'desc'  => '',
						'type'  => 'textarea',
						'placeholder'  => 'Descrição',
											
					),
					
					array(
						'name'  => 'Imagem: ',
						'id'    => "{$prefix}imagem_quemSomos2",						
						'type'  => 'image',
						'max_file_uploads'  => 1,						
					),				
					
				),
				
			);

			// METABOX QUEM SOMOS ÁREA 3
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxQuemSomos3',
				'title'			=> 'Detalhes da página terceira área de texto ',				
				'pages' 		=> array( 'page' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					
					array(
						'name'  => 'segunda área de texto: Título ',
						'id'    => "{$prefix}titulo_quemSomos3",
						'desc'  => '',
						'type'  => 'text',
						'placeholder'  => 'Título',
											
					),

					array(
						'name'  => 'segunda área de texto: Texto ',
						'id'    => "{$prefix}descricao_quemSomos3",
						'desc'  => '',
						'type'  => 'textarea',
						'placeholder'  => 'Descrição',
											
					),
					
					array(
						'name'  => 'Imagem: ',
						'id'    => "{$prefix}imagem_quemSomos3",						
						'type'  => 'image',
						'max_file_uploads'  => 1,						
					),				
					
				),
				
			);

			// METABOX PÁGINA DE ADVOGADOS
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxAdvogados',
				'title'			=> 'Detalhes da página ',				
				'pages' 		=> array( 'page' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					
					array(
						'name'  => 'Frase da foto banner ',
						'id'    => "{$prefix}advogados_frase",
						'desc'  => '',
						'type'  => 'text',
						
											
					),									
					
				),
				
			);

			// METABOX PÁGINA DE CONTATO
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxContato',
				'title'			=> 'Detalhes da página ',				
				'pages' 		=> array( 'page' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					
					array(
						'name'  => 'Frase da foto banner ',
						'id'    => "{$prefix}contato_frase",
						'desc'  => '',
						'type'  => 'text',
						
											
					),	

					array(
						'name'  => 'Nome dos advogados ',
						'id'    => "{$prefix}contato_nome",
						'desc'  => '',
						'type'  => 'text',
						
											
					),									
													
					
				),
				
			);

			

		
			return $metaboxes;
		}

		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesBsra(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerBsra(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseBsra');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseBsra();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );