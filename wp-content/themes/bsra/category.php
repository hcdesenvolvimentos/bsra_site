<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Dat BSRA
 */
global $configuracao;
get_header(); ?>

<div class="pg pg-blog">

	
	<!-- BANNER  -->
	<div class="bannerGeral" style="background:url(<?php  echo $configuracao['opt-blog-foto']['url'] ?>)">
		<div class="lenteBanner">
			<div class="bannerGeralInfor">
				<span><?php  echo $configuracao['opt-blog-titulo'] ?></span>
				<p><?php  echo $configuracao['opt-blog-frase'] ?></p>
			</div>
		</div>
	</div>

	<div class="container areaPostagem">	
		<section class="row">				
			
			<!-- SIDEBAR -->
			<div class="col-lg-4 col-md-3 sidebar">
				<img src="<?php bloginfo('template_directory'); ?>/img/calendar.jpg" alt="" class="img-responsive">
				<ul class="listaCategorias">
					<?php
						// CATEGORIA ATUAL
						$categoriaAtual = get_the_category();
						$categoriaAtual = $categoriaAtual[0]->cat_name;
						// LISTA DE CATEGORIAS
						$arrayCategorias = array();
						$categorias=get_categories($args);

						foreach($categorias as $categoria) {
							$arrayCategorias[$categoria->cat_ID] = $categoria->name;
							$nomeCategoria = $arrayCategorias[$categoria->cat_ID];

							if ($categoriaAtual == $nomeCategoria ) {
								
							
					?>
						<li class="categoria "><a class="ativa" href="<?php echo get_category_link($categoria->cat_ID); ?>"><?php echo $nomeCategoria; ?></a></li>

					<?php }else{ ?>
							<li class="categoria"><a href="<?php echo get_category_link($categoria->cat_ID); ?>"><?php echo $nomeCategoria; ?></a></li>

					<?php }} ?>

				</ul>
			</div>
			
			<!-- LISTA POSTAGENS -->
			<ul class="col-lg-8 col-md-9 listaPosts">

				<!-- CAMINHO DA PÁGINA -->
				<div class="caminho">Blog </div>

				<?php if ( have_posts() ) : 

						while ( have_posts() ) : the_post();
					
	                        $foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	                        $foto = $foto[0];

				?>
							<!-- NOVIDADE -->
							<li class="post">

								<!-- LINK NOVIDADE -->
								<a href="<?php echo get_permalink(); ?>" title="">

									<div class="foto" style="background: rgba(0,0,0,0.6) url(<?=$foto?>) no-repeat center center;"></div>
									<p class="data"><?php the_time('j F \d\e Y') ?></p>
									<p class="resumo">
										<?php
											$content = get_the_content();
											$conteudo = substr($content, 0, 100).'...';
											echo $conteudo;
										?>
									</p>
								</a>

							</li>

					<?php endwhile; ?>  

				<?php endif; ?>

			</ul>
		</section>

	</div>

</div>

<?php get_footer(); ?>
