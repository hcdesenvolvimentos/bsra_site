<?php
/**
 * Template Name: Profissionais
 * Description: 
 *
 * @package BSRA
 */
$fraseBanner =  rwmb_meta('Bsra_advogados_frase');
$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$foto = $foto[0];
get_header(); ?>

<div class="pg pg-proficionais">
		<!-- BANNER  -->
		<div class="bannerGeral" style="background:url(<?php echo $foto ?>)">
			<div class="lenteBanner">
				<div class="bannerGeralInfor">
					<span><?php echo get_the_title() ?></span>
					
					<p><?php  echo $fraseBanner ?></p>
				</div>
			</div>
		</div>

		<div class="container">
			
			<div class="texto">
				<p><?php echo get_the_content() ?></p>
			</div>

			<?php 
				$i = 0 ;
				// LOOP DE PROFISSIONAIS 
				$profissionais = new WP_Query( array( 'post_type' => 'profissionais', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
				while ( $profissionais->have_posts() ) : $profissionais->the_post();
					$profissionaisFoto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$profissionaisFoto = $profissionaisFoto[0];

					// DESCRIÇÃO
					$profissionais_infra =  rwmb_meta('Bsra_profissionais_infra');
					$profissionais_facebook =  rwmb_meta('Bsra_profissionais_facebook');
					$profissionais_linkedin =  rwmb_meta('Bsra_profissionais_linkedin');
					

					$nome = get_the_title();
							
			?>
			<div class="areaAdvogados">
				<img src="<?php echo $profissionaisFoto ?>" class="img-responsive" alt="">
				
				<div class="infoNome">
					<h2><?php echo $nome  ?></h2>
					<strong>Advogado (a)</strong>
					<span><?php echo $profissionais_infra ?></span>
				</div>

				<div class="redesSociais">
					<a href="<?php echo $profissionais_facebook ?>" target="_blank"alt="<?php echo get_the_title() ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a>
					<a href="<?php echo $profissionais_linkedin ?>" target="_blank"alt="<?php echo get_the_title() ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
				</div>
			</div>
			<?php endwhile; wp_reset_query(); ?>
		</div>
	</div>


<?php get_footer(); ?>